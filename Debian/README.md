# Debian setup after installation


1. ADD Sudo Users
```makefile
add-sudo:
    su -
    usermod -aG sudo $USER
    exit
```

2. Upgrade to testing
```makefile
upgrade-testing:
    sudo apt update
    sudo apt upgrade
    sudo bash -c 'echo "deb http://mirror.xtom.com.hk/debian/ testing main contrib non-free" > sources.list'
    sudo apt update
    sudo apt upgrade
    sudo apt upgrade --fix-missing
    sudo apt dist-upgrade
    sudo apt autoremove
```

3. Add repository
```makefile
add-debian-repo:
	# There are following repo added
	# 1. non Free contrib
	# 2. Backports
	# 3. Python
	# 4. Utilities
	# 5. Ansible
	# 6. Golang
	# 7. Flatpak
	# 8. Antix and MX linux
	# 9. Deepin Linux
	# 10. Cassandra
	# GUI apps
	# 1. Celluloid
	# 2. Chrome
	# 3. Vivaldi
	sudo apt install software-properties-common curl python-software-properties
	sudo apt-add-repository non-free
	sudo apt-add-repository contrib
	sudo sh -c 'echo "deb [trusted=yes] http://deb.debian.org/debian buster-backports main" >> /etc/apt/sources.list.d/backports.list'
	sudo add-apt-repository ppa:deadsnakes/ppa
	sudo add-apt-repository ppa:jerem-ferry/rust
	sudo apt-add-repository ppa:ansible/ansible
	sudo apt-add-repository ppa:longsleep/golang-backports
	sudo add-apt-repository ppa:alexlarsson/flatpak
	sudo sh -c 'echo "deb [trusted=yes] http://la.mxrepo.com/mx/repo/ buster main non-free" >> /etc/apt/sources.list.d/mx.list'
  	sudo sh -c 'echo "deb [trusted=yes] http://repo.antixlinux.com/buster buster main" >> /etc/apt/sources.list.d/mx.list'
  	sudo sh -c 'echo "deb [trusted=yes] http://www.deb-multimedia.org buster main non-free" >> /etc/apt/sources.list.d/mx.list'
	sudo sh -c 'echo "deb [trusted=yes] http://packages.deepin.com/deepin stable main contrib non-free" >> /etc/apt/sources.list.d/deepin.list'
	curl https://www.apache.org/dist/cassandra/KEYS | sudo apt-key add -
	sudo sh -c 'echo "deb http://www.apache.org/dist/cassandra/debian 39x main" >> /etc/apt/sources.list.d/cassandra.sources.list'
	# GUI APPS
	sudo add-apt-repository ppa:xuzhen666/gnome-mpv
	wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
	sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
	sudo add-apt-repository ppa:gnumdk/lollypop
	wget -qO- https://repo.vivaldi.com/archive/linux_signing_key.pub | sudo apt-key add
	sudo add-apt-repository 'deb https://repo.vivaldi.com/archive/deb/ stable main
	sudo apt update
```
4. Add command line packages
```makefile
add-cmd-packages:
	# Essential apps
	sudo apt install python3.8 python3-pip git ppa-purge
	# Essential Multimedia and downloader
	sudo apt install axel aria2c curl wget ffmpeg 
	# Multimedia Codecs
	sudo apt install libavcodec-extra ttf-mscorefonts-installer gstreamer1.0-libav gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-vaapi
	# Essential Zipping libraries
	sudo apt install zlib1g zlib1g-dev p7zip-rar p7zip-full unrar zip unzip
	# Other zipping libraries
	sudo apt install  unace sharutils rar uudeview libpcre3 libpcre3-dev mpack arj cabextract build-essential 
	# For SSL websites
	sudo apt install apt-transport-https openssl git openssh-server ca-certificates libssl-dev
```
5. Add GUI apps package manager
```
add-graphical-packager:
	# We then install 2 package manager
	sudo apt install python3.8 python3-pip
	sudo apt install snapd flatpak
	flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
	# TODO: Appimage luancher: wget "https://github.com/TheAssassin/AppImageLauncher/releases/download/v2.0.3/appimagelauncher_2.0.3-travis888.4f7bc8e.bionic_amd64.deb
    # TODO: Pling shop
```

```
_Second Snapshot_

5. UEFI Bugs in virtualbox, debian
    5.1 method 1
```sh
fs0:
cd \efi
ls
#Check the name of folder entry
cd $DISTRONAME
#locate grubx64.efi
cd ../
cd ../
echo '\path\to\grubx64.efi' >> startup.nsh
#eg \efi\debian\grubx64.efi
reset
```

5.2 method 2
```sh
sudo -i
mkdir -p /boot/efi/EFI/boot
touch /boot/efi/EFI/boot/bootx64.efi
cp /boot/efi/EFI/grubx64.efi /boot/efi/EFI/boot/bootx64.efi
```

6. Install other software
```sh
sudo apt install snapd ffmpeg nodejs sqlite tree yarn smplayer software-properties-common python3-pip3 
```
7 Install repo software
```sh
wget -qO- https://repo.vivaldi.com/archive/linux_signing_key.pub | sudo apt-key add -
sudo add-apt-repository 'deb https://repo.vivaldi.com/archive/deb/ stable main'
sudo apt update && sudo apt install vivaldi-stable

```

8. pip3 install
```sh
sudo pip3 install pillow youtube_dl scipy scikit-image requests pylint opencv-python
```

# Purpose for this script
* This script is used for optimizing gnome in a clean install by serveral means below

# Supressing RAM usge
* This script achieved to supress Ubuntu/debian gnome RAM usage from 2Gigs to 600-900mb idle,depends on the system monitor using.
![RAM](Debian/Gnome/Ram.jpeg)
# Decrease memory leak
* Although the tesing environment is Gnome 3.28, the script acheived to reduce memory leak by removing many bloat gnome installed

# Better defaults than gnome defualts
* Stronger defaults is installed for U, rather than the old-school one installed by gnome
* Music Player:lollypop
* Video Player: celluloid
* Software Center Frontend: Bauh, Synaptic
* Software Center Backends: Flatpak, Snap aptitude
* Browser : chrome and vivaldi repo added.Then there are firefox, chrome, vivaldi for you to choose
* Graphical driver option provided in scripts, will add detection later

# TODO
* Nonfree network driver by defaults
* Add theme center by default
* Install appimage to application by default
* Specialized config for Gamer and programmers
* Install a neat chrome shell theme by default
